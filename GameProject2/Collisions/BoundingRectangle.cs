﻿using Microsoft.Xna.Framework;

namespace GameProject2.Collisions
{
    /// <summary>
    /// A bounding rectangle for collision detection.
    /// </summary>
    public struct BoundingRectangle
    {
        public float X, Y;
        public float Width, Height;
        public float Left => X;
        public float Right => X + Width;
        public float Top => Y;
        public float Bottom => Y + Height;

        public BoundingRectangle(float x, float y, float width, float height)
        {
            X = x;
            Y = y;
            Width = width;
            Height = height;
        }

        public BoundingRectangle(Vector2 position, float width, float height)
        {
            X = position.X;
            Y = position.Y;
            Width = width;
            Height = height;
        }

        public bool CollidesWith(BoundingRectangle other)
        {
            return CollisionHelper.Collide(this, other);
        }

        public bool CollidesWith(BoundingCircle other)
        {
            return CollisionHelper.Collide(this, other);
        }
    }
}