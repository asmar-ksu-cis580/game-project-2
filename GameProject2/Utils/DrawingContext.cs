﻿using Microsoft.Xna.Framework.Graphics;

namespace GameProject2.Utils
{
    public struct DrawingContext
    {
        public SpriteBatch SpriteBatch { get; init; }
        
        public GraphicsDevice GraphicsDevice { get; init; }
    }
}