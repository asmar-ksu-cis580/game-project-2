﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GameProject2.Utils;

public struct UpdateContext
{
    public GameTime GameTime { get; init; }
    public GraphicsDevice GraphicsDevice { get; init; }
}