﻿using Microsoft.Xna.Framework;

namespace GameProject2.Particles;

public interface IParticleEmitter
{
    public Vector2 Position { get; }

    public Vector2 Velocity { get; }
}