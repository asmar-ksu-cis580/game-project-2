﻿using GameProject2.Utils;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GameProject2.Particles;

public class PixieParticleSystem : ParticleSystem
{
    private IParticleEmitter _emitter;

    public PixieParticleSystem(Game game, IParticleEmitter emitter) : base(game, 200)
    {
        _emitter = emitter;
    }

    protected override void InitializeConstants()
    {
        textureFilename = "particle";
        minNumParticles = 2;
        maxNumParticles = 5;
        blendState = BlendState.Additive;
        DrawOrder = AdditiveBlendDrawOrder;
    }

    protected override void InitializeParticle(ref Particle p, Vector2 where)
    {
        var velocity = _emitter.Velocity;
        var acceleration = Vector2.UnitY * 400;
        float scale = RandomHelper.NextFloat(0.1f, 0.5f);
        float lifetime = RandomHelper.NextFloat(0.1f, 1.0f);
        p.Initialize(where, velocity, acceleration, Color.Goldenrod, scale: scale, lifetime: lifetime);
    }

    public override void Update(GameTime gameTime)
    {
        base.Update(gameTime);
        AddParticles(_emitter.Position);
    }
}