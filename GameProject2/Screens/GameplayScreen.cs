﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading;
using GameProject2.Entities;
using GameProject2.Particles;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using GameProject2.StateManagement;
using GameProject2.Tiles;
using GameProject2.Utils;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;

namespace GameProject2.Screens
{
    // This screen implements the actual game logic. It is just a
    // placeholder to get the idea across: you'll probably want to
    // put some more interesting gameplay in here!
    public class GameplayScreen : GameScreen, IParticleEmitter
    {
        private Game _game;

        private ContentManager _content;
        private SpriteFont _gameFont;

        private float _pauseAlpha;
        private readonly InputAction _pauseAction;

        private SoundEffect starPickupSound;
        private Song backgroundMusic;

        private Puck _puck;
        private List<Comet> _comets = new List<Comet>();
        // private List<Wall> _walls = new List<Wall>();

        private const int ShakingTime = 600;
        private const int ShakingMagnitude = 4;
        private float _shakingTime = 0;
        private Matrix _shakingMatrix = Matrix.Identity;

        private const int TintTime = 1000;
        private float _tintTime = 0;

        public Vector2 Position { get; set; }
        public Vector2 Velocity { get; set; }

        private BasicTilemap _tilemap;

        public const string SaveFilename = "save.txt";
        private int _score;

        private (Vector2, Vector2) GetNewCometParameters()
        {
            var viewport = ScreenManager.Graphics.GraphicsDevice.Viewport;
            var position = Vector2Helper.Random();
            position.X = position.X * (viewport.Width - Comet.RenderWidth) + Comet.RenderWidth / 2;
            position.Y = position.Y * (viewport.Height - Comet.RenderHeight) + Comet.RenderHeight / 2;
            var direction = Vector2Helper.Random();
            direction.Normalize();
            return (direction, position);
        }

        public GameplayScreen(Game game)
        {
            _game = game;

            TransitionOnTime = TimeSpan.FromSeconds(1.5);
            TransitionOffTime = TimeSpan.FromSeconds(0.5);

            _pauseAction = new InputAction(
                new[] { Buttons.Start, Buttons.Back },
                new[] { Keys.Back, Keys.Escape }, true
            );
        }

        // Load graphics content for the game
        public override void Activate()
        {
            if (_content == null)
                _content = new ContentManager(ScreenManager.Game.Services, "Content");

            _gameFont = _content.Load<SpriteFont>("gamefont");

            // A real game would probably have more content than this sample, so
            // it would take longer to load. We simulate that by delaying for a
            // while, giving you a chance to admire the beautiful loading screen.
            Thread.Sleep(1000);

            var viewport = ScreenManager.Graphics.GraphicsDevice.Viewport;

            {
                var position = Vector2Helper.Random();
                position.X = position.X * (viewport.Width - Puck.RenderResolution) + Puck.RenderResolution / 2;
                position.Y = position.Y * (viewport.Height - Puck.RenderResolution) + Puck.RenderResolution / 2;
                _puck = new Puck(Vector2.Zero, position);
            }
            for (int i = 0; i < 2; ++i)
            {
                var (direction, position) = GetNewCometParameters();
                _comets.Add(new Comet(direction, position));
            }

            _puck.LoadContent(_content);
            foreach (var comet in _comets)
            {
                comet.LoadContent(_content);
            }

            // for (int i = 0; i < 2; ++i)
            // {
            //     var wall = new Wall();
            //     wall.LoadContent(_content);
            //     wall.Position = Vector2Helper.Random();
            //     wall.Position.X *= viewport.Width - Wall.RenderWidth;
            //     wall.Position.Y *= viewport.Height - Wall.RenderHeight;
            //     _walls.Add(wall);
            // }

            var pixieParticleSystem = new PixieParticleSystem(_game, this);
            _game.Components.Add(pixieParticleSystem);

            backgroundMusic = _content.Load<Song>("DeeYan-Key-TheGame");
            MediaPlayer.IsRepeating = true;
            MediaPlayer.Play(backgroundMusic);

            starPickupSound = _content.Load<SoundEffect>("Pickup_Coin15");

            _tilemap = _content.Load<BasicTilemap>("tilemap");

            _score = File.Exists(SaveFilename)
                ? int.TryParse(File.ReadAllText(SaveFilename), out int score) ? score : 0
                : 0;

            // once the load has finished, we use ResetElapsedTime to tell the game's
            // timing mechanism that we have just finished a very long frame, and that
            // it should not try to catch up.
            ScreenManager.Game.ResetElapsedTime();
        }


        public override void Deactivate()
        {
            base.Deactivate();
        }

        public override void Unload()
        {
            _content.Unload();
        }

        // This method checks the GameScreen.IsActive property, so the game will
        // stop updating when the pause menu is active, or if you tab away to a different application.
        public override void Update(GameTime gameTime, bool otherScreenHasFocus, bool coveredByOtherScreen)
        {
            base.Update(gameTime, otherScreenHasFocus, false);

            // Gradually fade in or out depending on whether we are covered by the pause screen.
            if (coveredByOtherScreen)
                _pauseAlpha = Math.Min(_pauseAlpha + 1f / 32, 1);
            else
                _pauseAlpha = Math.Max(_pauseAlpha - 1f / 32, 0);

            if (IsActive)
            {
                var mouseState = Mouse.GetState();
                var viewport = ScreenManager.Graphics.GraphicsDevice.Viewport;

                var updateContext = new UpdateContext
                {
                    GameTime = gameTime,
                    GraphicsDevice = ScreenManager.Graphics.GraphicsDevice,
                };

                {
                    var newDirection = mouseState.Position.ToVector2() - _puck.Center;
                    newDirection.X /= viewport.Width;
                    newDirection.Y /= viewport.Height;
                    newDirection *= 15;
                    _puck.Direction = newDirection;
                    _puck.Update(updateContext);
                }
                foreach (var comet in _comets)
                {
                    comet.Update(updateContext);
                }

                if (_shakingTime > 0)
                {
                    _shakingTime -= (float)gameTime.ElapsedGameTime.TotalMilliseconds;
                    _shakingMatrix = Matrix.CreateTranslation(
                        ShakingMagnitude * MathF.Sin(_shakingTime),
                        ShakingMagnitude * MathF.Cos(_shakingTime),
                        0
                    );
                }
                else
                {
                    _shakingMatrix = Matrix.Identity;
                }

                if (_tintTime > 0)
                {
                    _tintTime -= (float)gameTime.ElapsedGameTime.TotalMilliseconds;
                    _puck.Tint = Color.Yellow;
                }
                else
                {
                    _puck.Tint = null;
                }

                foreach (var comet in _comets)
                {
                    if (_puck.Bounds.CollidesWith(comet.Bounds))
                    {
                        starPickupSound.Play();
                        var (direction, position) = GetNewCometParameters();
                        comet.Direction = direction;
                        comet.Position = position;
                        _shakingTime = ShakingTime;
                        _tintTime = TintTime;
                        ++_score;
                        File.WriteAllText(SaveFilename, _score.ToString());
                    }
                }
            }

            var mousePosition = Mouse.GetState().Position.ToVector2();
            Velocity = mousePosition - Position;
            Position = mousePosition;
        }

        // Unlike the Update method, this will only be called when the gameplay screen is active.
        public override void HandleInput(GameTime gameTime, InputState input)
        {
            if (input == null)
                throw new ArgumentNullException(nameof(input));

            // Look up inputs for the active player profile.
            var playerIndex = (int)ControllingPlayer.Value;

            var keyboardState = input.CurrentKeyboardStates[playerIndex];
            var gamePadState = input.CurrentGamePadStates[playerIndex];

            // The game pauses either if the user presses the pause button, or if
            // they unplug the active gamepad. This requires us to keep track of
            // whether a gamepad was ever plugged in, because we don't want to pause
            // on PC if they are playing with a keyboard and have no gamepad at all!
            bool gamePadDisconnected = !gamePadState.IsConnected && input.GamePadWasConnected[playerIndex];

            PlayerIndex player;
            if (_pauseAction.Occurred(input, ControllingPlayer, out player) || gamePadDisconnected)
            {
                ScreenManager.AddScreen(new PauseMenuScreen(_game), ControllingPlayer);
            }
            else
            {
            }
        }

        public override void Draw(GameTime gameTime)
        {
            // This game has a blue background. Why? Because!
            ScreenManager.GraphicsDevice.Clear(ClearOptions.Target, Color.CornflowerBlue, 0, 0);

            // Our player and enemy are both actually just text strings.
            var spriteBatch = ScreenManager.SpriteBatch;

            var drawingContext = new DrawingContext
            {
                SpriteBatch = spriteBatch,
                GraphicsDevice = ScreenManager.Graphics.GraphicsDevice,
            };

            spriteBatch.Begin(transformMatrix: _shakingMatrix);

            _tilemap.Draw(drawingContext);

            // foreach (var wall in _walls)
            // {
            //     wall.Draw(drawingContext);
            // }

            _puck.Draw(drawingContext);

            foreach (var comet in _comets)
            {
                comet.Draw(drawingContext);
            }

            spriteBatch.DrawString(_gameFont, $"Score: {_score}", new Vector2(32, 32), Color.White);

            spriteBatch.End();

            // If the game is transitioning on or off, fade it out to black.
            if (TransitionPosition > 0 || _pauseAlpha > 0)
            {
                float alpha = MathHelper.Lerp(1f - TransitionAlpha, 1f, _pauseAlpha / 2);

                ScreenManager.FadeBackBufferToBlack(alpha);
            }
        }
    }
}