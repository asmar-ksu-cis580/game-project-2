﻿using GameProject2.Utils;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace GameProject2.Entities;

public class Wall : Entity
{
    private Texture2D _texture;

    private float _scale;
    public const float RenderWidth = 514f;
    public const float RenderHeight = RenderWidth / 1028f * 256f;

    public Vector2 Position = Vector2.Zero;

    public void LoadContent(ContentManager contentManager)
    {
        _texture = contentManager.Load<Texture2D>("BrickWall04_1028x256");
        _scale = RenderWidth / _texture.Width;
    }

    public void Draw(DrawingContext drawingContext)
    {
        drawingContext.SpriteBatch.Draw(
            _texture,
            Position,
            null,
            Color.White,
            0f,
            Vector2.Zero,
            _scale,
            SpriteEffects.None,
            0f
        );
    }
}