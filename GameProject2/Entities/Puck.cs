﻿using GameProject2.Collisions;
using GameProject2.Utils;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace GameProject2.Entities
{
    public class Puck : Entity
    {
        private Texture2D _texture;

        public Vector2 Direction;
        private Vector2 _center;

        public Color? Tint = null;

        public Vector2 Center
        {
            get => _center;
            set
            {
                _center = value;
                Bounds.Center = value;
            }
        }

        public float Rotation = 0f;

        private float _scale;
        public const float RenderResolution = 256f;

        public BoundingCircle Bounds;

        public Puck(Vector2 direction, Vector2 center)
        {
            Direction = direction;
            Center = center;
            Bounds = new BoundingCircle(center, RenderResolution / 2);
        }

        public void LoadContent(ContentManager contentManager)
        {
            _texture = contentManager.Load<Texture2D>("puck1024");
            _scale = RenderResolution / _texture.Width;
        }

        public void Update(UpdateContext updateContext)
        {
            var viewport = updateContext.GraphicsDevice.Viewport;

            var totalSeconds = (float)updateContext.GameTime.ElapsedGameTime.TotalSeconds;
            Rotation += totalSeconds;

            float axisDelta = totalSeconds * 100;
            {
                Center += axisDelta * Direction;
                float x = MathHelper.Clamp(Center.X, RenderResolution / 2, viewport.Width - RenderResolution / 2);
                float y = MathHelper.Clamp(Center.Y, RenderResolution / 2, viewport.Height - RenderResolution / 2);
                Center = new Vector2(x, y);
            }
        }

        public void Draw(DrawingContext context)
        {
            var origin = new Vector2((float)_texture.Width / 2, (float)_texture.Height / 2);
            context.SpriteBatch.Draw(
                _texture,
                Center,
                null,
                Tint ?? Color.White,
                Rotation,
                origin,
                _scale,
                SpriteEffects.None,
                0
            );
        }
    }
}