﻿using System;
using GameProject2.Collisions;
using GameProject2.Utils;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace GameProject2.Entities
{
    public class Comet : Entity
    {
        private Texture2D _texture;

        public Vector2 Direction;
        private Vector2 _position;

        public Vector2 Position
        {
            get => _position;
            set
            {
                _position = value;
                Bounds.X = value.X;
                Bounds.Y = value.Y;
            }
        }

        public float Rotation = 0f;

        private float _scale;
        public const float RenderWidth = 160f;
        public const float RenderHeight = RenderWidth / 96 * 38;

        private AnimationFrame _animationFrame = AnimationFrame.Plain;
        private double _animationTimer = 0;
        public const int NumberOfFrames = 3;
        public const double FrameDuration = 0.65;

        private enum AnimationFrame
        {
            Plain,
            Partial,
            Full
        }

        private static AnimationFrame NextFrame(AnimationFrame animationFrame)
        {
            return animationFrame switch
            {
                AnimationFrame.Plain => AnimationFrame.Partial,
                AnimationFrame.Partial => AnimationFrame.Full,
                AnimationFrame.Full => AnimationFrame.Plain,
                _ => throw new ArgumentOutOfRangeException(nameof(animationFrame), animationFrame, null)
            };
        }

        private static int FrameOffset(AnimationFrame animationFrame)
        {
            return animationFrame switch
            {
                AnimationFrame.Plain => 2,
                AnimationFrame.Partial => 1,
                AnimationFrame.Full => 0,
                _ => throw new ArgumentOutOfRangeException(nameof(animationFrame), animationFrame, null)
            };
        }

        public BoundingRectangle Bounds;

        public Comet(Vector2 direction, Vector2 position)
        {
            Bounds = new BoundingRectangle(Position, RenderWidth, RenderHeight);
            Direction = direction;
            Position = position;
        }

        public void LoadContent(ContentManager contentManager)
        {
            _texture = contentManager.Load<Texture2D>("cometSprite");
            _scale = RenderWidth / _texture.Width * NumberOfFrames;
        }

        public void Update(UpdateContext updateContext)
        {
            var viewport = updateContext.GraphicsDevice.Viewport;

            var totalSeconds = (float)updateContext.GameTime.ElapsedGameTime.TotalSeconds;
            _animationTimer += updateContext.GameTime.ElapsedGameTime.TotalSeconds;
            if (_animationTimer > FrameDuration)
            {
                _animationFrame = NextFrame(_animationFrame);
                _animationTimer -= FrameDuration;
            }

            float axisDelta = totalSeconds * 125;
            Position += axisDelta * Direction;

            if (Position.X <= -RenderWidth)
            {
                Position = new Vector2(viewport.Width, Position.Y);
            }

            if (Position.X >= viewport.Width + RenderWidth)
            {
                Position = new Vector2(-RenderWidth, Position.Y);
            }

            if (Position.Y <= -RenderHeight)
            {
                Position = new Vector2(Position.X, viewport.Height);
            }

            if (Position.Y >= viewport.Height + RenderHeight)
            {
                Position = new Vector2(Position.X, -RenderHeight);
            }
        }

        public void Draw(DrawingContext context)
        {
            // var origin = new Vector2((float)_texture.Width / 2, (float)_texture.Height / 2);
            var origin = new Vector2(0, 0);
            int singleWidth = _texture.Width / NumberOfFrames;
            int singleHeight = _texture.Height;
            var sourceRectangle =
                new Rectangle(FrameOffset(_animationFrame) * singleWidth, 0, singleWidth, singleHeight);
            context.SpriteBatch.Draw(
                _texture,
                Position,
                sourceRectangle,
                Color.White,
                0,
                origin,
                _scale,
                SpriteEffects.None,
                0
            );
        }
    }
}